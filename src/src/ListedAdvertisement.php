<?php

namespace App;

class ListedAdvertisement
{
    public function __construct(
        public string $title,
        public string $url
    ) {
    }
}