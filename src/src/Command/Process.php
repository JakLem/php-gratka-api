<?php

namespace App\Command;

use Ramsey\Uuid\UuidInterface;

class Process {
    private const RANGES = [
        [1, 100],
        [101, 200],
        [201, 300],
    ];
    public function __construct(
        private UuidInterface $processId,
        private int $order,
    ) {
    }
    public function range(): array
    {
        return self::RANGES[$this->order];
    }
};
