<?php

namespace App\Command;

use App\Advertisement\Application\Command\GetAdvertisementCommand;
use App\Advertisement\Application\ListCrawler;
use App\Shared\Application\Exception\CannotConnectToExternalUrlException;
use League\Tactician\CommandBus;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\CacheItem;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Ramsey\Uuid\UuidInterface;
use Ramsey\Uuid\Uuid;

#[AsCommand(
    name: 'app:get-adv-gratka',
    description: 'Getting advs from list',
)]
class GetAdvGratkaCommand extends Command
{
    private FilesystemAdapter $cache;

    public function __construct(
       string $name = null,
       private CommandBus $commandBus,
   ) {
       parent::__construct($name);
       $this->cache = new FilesystemAdapter();
   }

    protected function configure(): void
    {
        $this
            ->addArgument('clear', InputArgument::OPTIONAL, 'Argument description', false)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $processId = Uuid::uuid4();
        // założenia:
        // zawsze 3 prcesy, ktore biorą sztywną wartość 100
        $processItemsKey = 'processes.list';
        if ($input->getArgument('clear')) {
            $this->cache->clear($processItemsKey);
            die;
        }

        $addToFromActiveProcesses = function (string $processItemsKey, UuidInterface $processId) {
            /**
             * @var CacheItem $cacheProcessesList
             */
            $cacheProcessesList = $this->cache->getItem($processItemsKey);

            $oldList = $cacheProcessesList->get();
            if (is_null($oldList)) {
                $cacheProcessesList->set([]);
                $oldList = $cacheProcessesList->get();
            }
            if (count($oldList) >= 3) {
                die;
            }

            $process = new Process($processId, count($cacheProcessesList->get()));
            $cacheProcessesList->set(array_merge($oldList, [
                    $processId->toString() => $process
                ]
            ));
            $this->cache->save($cacheProcessesList);

            return $process;
        };

        $getOutFromActiveProcesses = function (string $processItemsKey, UuidInterface $processId) {
            $cacheProcessesList = $this->cache->getItem($processItemsKey);
            $list = $cacheProcessesList->get();
            if (in_array($processId, $list)) {
                unset($list[$processId->toString()]);
                $cacheProcessesList->set($list);
            }

            $this->cache->save($cacheProcessesList);
        };

        $process = $addToFromActiveProcesses($processItemsKey, $processId);
        $range = $process->range();
        $advNumber = 0;
        for ($i = $range[0] ; $i < $range[1] ; $i++) {
            try {
                $listCrawler = new ListCrawler(sprintf("https://gratka.pl/nieruchomosci?page=%d", $i));
                $list = $listCrawler->getList();
            } catch (CannotConnectToExternalUrlException $e) {
                $io->warning($e->getMessage());
                $i--;
                $io->warning("program stopped because of problem with crawl 'Gratka' advertisement");
                $this->askForContinue($input, $output);
                continue;
            }

            foreach ($list as $oneListed)
            {
                $advNumber++;
                $command = new GetAdvertisementCommand($oneListed->url);
                $ad = $this->commandBus->handle($command);

                $io->info("Page {$i} - Advertisement ({$advNumber}) title " . $ad->title);
            }
            $io->success('Success page : ' . $i);


        }
        $getOutFromActiveProcesses($processItemsKey, $processId);
        $io->success('Success whole Gratka!');

        return Command::SUCCESS;
    }

    private function askForContinue(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $helper = $this->getHelper('question');
        $makeQuestion = fn (string $time) => new ConfirmationQuestion(sprintf('(%s) Continue? [y] : ', $time));
        $askQuestion = fn (Question $question) => $helper->ask($input, $output, $question);
        $answerReaction = function ($askQuestion) use ($io) {
            if (!$askQuestion) {
                $io->warning("end of program");
                die;
            }
        };

        $time = time();//(new \DateTime('now'))->getTimestamp();
        $strTime = date('d-m-Y H:i:s', $time);

        $answer = $askQuestion($makeQuestion($strTime));
        $answerReaction($answer);
    }
}
