<?php


namespace App\Shared\Application\Exception;


use Throwable;

class CannotConnectToExternalUrlException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function __toString(): string
    {
        return $this->message;
    }
}