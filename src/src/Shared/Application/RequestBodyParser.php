<?php


namespace App\Shared\Application;


use App\Shared\Application\Exception\InvalidBodyRequestPropertyException;
use Symfony\Component\HttpFoundation\Request;

class RequestBodyParser
{
    private array $bodyValues;

    public function __get(string $property)
    {
        if (!array_key_exists($property, $this->bodyValues)) {
            $message = sprintf("Property %s doesnt exist in request body.", $property);
            throw new InvalidBodyRequestPropertyException($message);
        }

        return $this->bodyValues[$property];
    }

    public function parse(Request $request)
    {
        $this->bodyValues =  json_decode($request->getContent(), true) ?? [];
    }
}