<?php


namespace App\Shared\Infrastructure;

use App\Shared\Application\Exception\CannotConnectToExternalUrlException;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\ParameterBag;

class SiteContentProvider
{
    const AGENT = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

    private string $html;
    private string $agent;
    private Crawler $crawler;

    /**
     * SiteContentProvider constructor.
     * @param string $url
     * @throws \Exception
     */
    public function __construct(
        private string $url
    )
    {
        $this->crawler = new Crawler();

        $container = new ParameterBag();
        $this->agent = $container->get("SITE_CONTENT_PROVIDER_AGENT", self::AGENT);
        $this->html = $this->getHtmlContent($this->url);
    }

    public function getFirstElementValue(string $element) : string
    {
        return $this->getElementValue($element)[0] ?? "";
    }

    public function getElementValue(string $element): array
    {
        $crawler = new Crawler($this->html);
        $nodeValues = $crawler->filter($element)->each(function (Crawler $node, $i) {
            return $node->text();
        });

        return $nodeValues;
    }

    public function getFirstElementAttributeValue(string $element, string $attr) : string
    {
        return $this->getElementAttributeValue($element, $attr)[0] ?? "";
    }

    public function getElementAttributeValue(string $element, string $attr): array
    {
        $crawler = new Crawler($this->html);
        $nodeValues = $crawler->filter($element)->each(function (Crawler $node, $i) use ($attr) {
            return $node->attr($attr);
        });

        return $nodeValues;
    }

    public function getByPregMatchRegex(string $regex) : array
    {
        preg_match($regex, $this->html, $match);
        return $match;
    }

    private function getHtmlContent(string $url): bool|string
    {
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_USERAGENT, $this->agent);
        $contents = curl_exec($ch);
        if (($err = curl_errno($ch))) {
            // dodać wyjątek z tym, że url jest błędny
           throw new CannotConnectToExternalUrlException(curl_error($ch));
        }
        curl_close($ch);
        return $contents;
    }
}