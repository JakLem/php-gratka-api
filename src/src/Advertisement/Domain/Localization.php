<?php


namespace App\Advertisement\Domain;


class Localization
{

    public function __construct(
        public string $city,
        public string $voivodeship,
        public string $street,
    )
    {
    }
}