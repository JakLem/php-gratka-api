<?php

namespace App\Advertisement\Domain;

interface AdvertisementProvider
{
    public function getByUrl(string $url);
}