<?php

namespace App\Advertisement\Domain;


class Advertisement
{
    public string $title;
    public string $description;
    public float $price;
    public string $currency;
    public string $phoneNumber;
    public ?Localization $localization;
}