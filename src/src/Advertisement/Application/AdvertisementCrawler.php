<?php

namespace App\Advertisement\Application;

use App\Shared\Infrastructure\SiteContentProvider;

class AdvertisementCrawler
{
    private SiteContentProvider $crawler;

    public function __construct(
         string $url
    )
    {
        $this->crawler = new SiteContentProvider($url);
    }

    public function getTitle() : string
    {
        return $this->crawler->getFirstElementValue(".sticker__info .sticker__title");
    }

    public function getDescription() : string
    {
        return $this->crawler->getFirstElementValue(".description__container .description__rolled");
    }

    public function getPrice() : float
    {
        $priceWithCurrency = $this->getPriceWithCurrency();
        $priceDataArray = explode(" ", $priceWithCurrency);
        array_pop($priceDataArray);
        return floatval(implode('', $priceDataArray));
    }

    public function getCurrency() : string
    {
        return $this->crawler->getFirstElementValue(".priceInfo .priceInfo__currency");
    }

    public function getPriceWithCurrency() : string
    {
        return $this->crawler->getFirstElementValue(".priceInfo .priceInfo__value");
    }

    public function getPhoneNumber() : string
    {
        return $this->crawler->getFirstElementAttributeValue(".phoneSmallButton__button", "data-full-phone-number");
    }

    public function getLocalization() : array
    {
        return $this->crawler->getByPregMatchRegex('/const addressObject =(.*?);/');
    }
}