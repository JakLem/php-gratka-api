<?php

namespace App\Advertisement\Application\Handler;

use App\Advertisement\Application\Command\GetAdvertisementCommand;
use App\Advertisement\Domain\AdvertisementProvider;

class GetAdvertisementHandler
{
    /**
     * @var AdvertisementProvider
     */
    private AdvertisementProvider $advertisementGetter;

    /**
     * GetAdvertisementHandler constructor.
     * @param AdvertisementProvider $advertisementGetter
     */
    public function __construct(AdvertisementProvider $advertisementGetter)
    {
        $this->advertisementGetter = $advertisementGetter;
    }

    public function handle(GetAdvertisementCommand $command)
    {
        return $this->advertisementGetter->getByUrl($command->url);
    }
}