<?php


namespace App\Advertisement\Application;


use App\Advertisement\Domain\Localization;
use JetBrains\PhpStorm\Pure;

class LocalizationFactory
{
    /**
     * @param array $localization
     * @return Localization
     */
    #[Pure] public function create(
        array $localization
    ) : Localization
    {
        $fixValue = fn ($arrayKey) => !empty($localization[$arrayKey]) ? $localization[$arrayKey] : "";

        $voivodeship = $fixValue("lokalizacja_region");
        $city = $fixValue("lokalizacja_miejscowosc");
        $street = $fixValue("lokalizacja_ulica");

        return new Localization($city, $voivodeship, $street);
    }
}