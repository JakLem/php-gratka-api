<?php

namespace App\Advertisement\Application\Command;


class GetAdvertisementCommand
{
    public function __construct(
        public string $url
    )
    {
    }
}