<?php

namespace App\Advertisement\Application;

use App\ListedAdvertisement;
use App\Shared\Infrastructure\SiteContentProvider;

class ListCrawler
{
    private SiteContentProvider $crawler;

    public function __construct(
        string $url
    )
    {
        $this->crawler = new SiteContentProvider($url);
    }

    public function getList(): array
    {
        $titles = $this->crawler->getElementValue(".listing__content .teaserUnified .teaserUnified__anchor");
        $urls = $this->crawler->getElementAttributeValue(".listing__content .teaserUnified .teaserUnified__anchor", "href");

        foreach ($titles as $key => $title) {
            $listedAdv = new ListedAdvertisement($title, $urls[$key]);
            $list[] = $listedAdv;
        }

        return $list;
    }
}