<?php


namespace App\Advertisement\Application\Facade;


use App\Advertisement\Application\LocalizationFactory;
use App\Advertisement\Domain\Localization;
use Symfony\Component\HttpKernel\Log\Logger;

class CreateLocalizationFacade
{

    public function createFromRegexMatchesArray(array $localizationDirtData) : ?Localization
    {
        if (isset($localizationDirtData[1])) {
            $localizationFactory = new LocalizationFactory();
            $localizationDirtDataArray = json_decode($localizationDirtData[1], true);

            return $localizationFactory->create($localizationDirtDataArray);
        }

        $logger = new Logger();
        $logger->info("Cannot get localization from advertisement.");

        return null;
    }
}