<?php


namespace App\Advertisement\Application\Builder;


use App\Advertisement\Application\AdvertisementCrawler;
use App\Advertisement\Application\Facade\CreateLocalizationFacade;
use App\Advertisement\Domain\Advertisement;

class AdvertisementFromHtmlBuilder implements AdvertisementBuilder
{
    /**
     * @var AdvertisementCrawler
     */
    private AdvertisementCrawler $crawler;
    /**
     * @var Advertisement
     */
    private Advertisement $advertisement;

    public function __construct(string $url)
    {
        $this->crawler = new AdvertisementCrawler($url);
        $this->advertisement = new Advertisement();
    }

    public function addTitle() : self
    {
        $this->advertisement->title = $this->crawler->getTitle();
        return $this;
    }

    public function addDescription() : self
    {
        $this->advertisement->description = $this->crawler->getDescription();
        return $this;
    }

    public function addPrice() : self
    {
        $this->advertisement->price = $this->crawler->getPrice();
        return $this;
    }

    public function addCurrency() : self
    {
        $this->advertisement->currency = $this->crawler->getCurrency();
        return $this;
    }

    public function addPhoneNumber() : self
    {
        $this->advertisement->phoneNumber = $this->crawler->getPhoneNumber();
        return $this;
    }

    public function addLocalization() : self
    {
        $localizationDirtData = $this->crawler->getLocalization();
        $createLocalizationFacade = new CreateLocalizationFacade();

        $this->advertisement->localization = $createLocalizationFacade->createFromRegexMatchesArray($localizationDirtData);
        return $this;
    }

    public function build(): Advertisement
    {
        return $this->advertisement;
    }
}