<?php


namespace App\Advertisement\Application\Builder;


use App\Advertisement\Domain\Advertisement;

interface AdvertisementBuilder
{
    public function addTitle() : self;
    public function addDescription() : self;
    public function addPrice() : self;
    public function addCurrency() : self;
    public function addPhoneNumber() : self;
    public function addLocalization() : self;

    public function build() : Advertisement;
}