<?php


namespace App\Advertisement\Infrastructure;


use App\Advertisement\Application\Builder\AdvertisementFromHtmlBuilder;
use App\Advertisement\Domain\Advertisement;
use App\Advertisement\Domain\AdvertisementProvider;

class AdvertisementGetter implements AdvertisementProvider
{
    public function getByUrl(string $url) : Advertisement
    {
        $builder = new AdvertisementFromHtmlBuilder($url);
        $advertisement =
            $builder
                ->addTitle()
                ->addDescription()
                ->addPrice()
                ->addCurrency()
                ->addPhoneNumber()
                ->addLocalization()
                ->build();

        return $advertisement;
    }
}