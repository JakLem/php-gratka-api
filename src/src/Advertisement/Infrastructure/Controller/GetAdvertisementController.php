<?php

namespace App\Advertisement\Infrastructure\Controller;


use App\Advertisement\Application\Command\GetAdvertisementCommand;
use App\Shared\Application\RequestBodyParser;
use League\Tactician\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GetAdvertisementController extends AbstractController
{
    public function __construct(
        private CommandBus $commandBus
    )
    {
    }

    //#[Route("/advertisement", name: "api_get_advertisement", methods: ["GET"])]
    public function getAdvertisement(
        Request $request,
    ): JsonResponse
    {
        $bodyParser = new RequestBodyParser();
        $bodyParser->parse($request);

        $command = new GetAdvertisementCommand($bodyParser->url);
        $ad = $this->commandBus->handle($command);

        return $this->json($ad);
    }
}